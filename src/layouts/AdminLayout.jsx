import MenuComponent from "../components/common/MenuComponent";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
const AdminLayout = ({children}) => {
    return (
        <div className={"container-fluid"}>
            <div className={"row"}>
                <div className={"col-md-3 d-md-block d-none"}>
                    <MenuComponent/>
                </div>
                <div className={"col-md-9 col-12"}>{children}</div>
            </div>
            <ToastContainer />
        </div>
    );
}

export default AdminLayout;