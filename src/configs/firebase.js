import { initializeApp } from 'firebase/app';
import { getDatabase } from "firebase/database";
const firebaseConfig = {
    apiKey: "AIzaSyAfKNUuDB61qCLLlN-y68uTPKA9tNtbTkw",
    authDomain: "bsm-light-vn.firebaseapp.com",
    databaseURL: "https://bsm-light-vn-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "bsm-light-vn",
    storageBucket: "bsm-light-vn.appspot.com",
    messagingSenderId: "572902198465",
    appId: "1:572902198465:web:9b95e480902d03f521d840",
    measurementId: "G-283HH5Q1Q1"
};

export const firebaseApp = initializeApp(firebaseConfig);

export const appDatabase = getDatabase();