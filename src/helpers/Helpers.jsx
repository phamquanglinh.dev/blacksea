export function objectToArray(object) {
    return Object.keys(object).map(key => ({
        id: key,
        ...object[key]
    }));
}

export function isExist(object, array) {
    return array.some(item => item.id === object.id);
}