export class UserModel {
    id
    username
    role
    profile_picture

    constructor({id, name, role, profile_picture}) {
        this.id = id
        this.name = name
        this.role = role
        this.profile_picture = profile_picture
    }
}