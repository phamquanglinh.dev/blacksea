import "../resources/css/app.css";
import AdminLayout from "../layouts/AdminLayout";
import {Button, Input} from "antd";
import {CommonServices} from "../services/CommonServices";
import {toast} from "react-toastify";
import {appDatabase} from "../configs/firebase";
import {useEffect, useState} from "react";
import {ref, onValue} from "firebase/database";
import {UserModel} from "../models/UserModel";
import {objectToArray} from "../helpers/Helpers";


const HomeScreen = () => {
    const [users, setUsers] = useState([])
    const [userData, setUserData] = useState({})

    useEffect(() => {
        const starCountRef = ref(appDatabase, 'users/');

        onValue(starCountRef, (records) => {
            if (records.exists()) {
                const usersData = [];

                objectToArray(records.val()).forEach((record,) => {
                    usersData.push(new UserModel(record))
                })

                setUsers(usersData)
            }
        });

    }, [appDatabase])

    function testFirebase() {
        const commonService = new CommonServices();
        commonService.writeUserData(userData).then(() => {
            toast('Success !', {});
        }).catch((e) => {
            console.log(e)
            toast('Failed !')
        })
    }

    return (
        <AdminLayout>
            <div className={"p-4"}>
                {users.map((user, key) =>
                    <div key={key}>
                        <div>ID: {user.id}</div>
                        <div>Username: {user.name}</div>
                        <div>Role: {user.role}</div>
                        <div>Profile: {user.profile_picture}</div>
                        <hr/>
                    </div>
                )}

                <hr/>
                <div className={"d-flex"}>
                    <Input
                        rootClassName={"col-md-3 me-2"}
                        placeHolder={"name"}
                        onChange={(r) => {
                            setUserData({...userData, name: r.target.value})

                        }}
                    />

                    <Input
                        rootClassName={"col-md-3 me-2"}
                        placeHolder={"role"}
                        onChange={(r) => {
                            setUserData({...userData, role: r.target.value})
                        }}
                    />

                    <Input
                        rootClassName={"col-md-3 me-2"}
                        placeHolder={"profile_picture"}
                        onChange={(r) => {
                            setUserData({...userData, profile_picture: r.target.value})
                        }}
                    />
                </div>

                <Button className={"mt-2"} onClick={testFirebase}>ADD</Button>
            </div>
        </AdminLayout>
    )
}

export default HomeScreen;