import * as React from "react";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import HomeScreen from "./screens/HomeScreen";
import ChatScreen from "./screens/ChatScreen";

const main_router = createBrowserRouter([
    {
        path: "/",
        element: <HomeScreen/>,
    },
    {
        path: "/chat",
        element: <ChatScreen/>,
    },
]);

function App() {
  return (
      <React.StrictMode>
          <RouterProvider router={main_router} />
      </React.StrictMode>
  );
}

export default App;
