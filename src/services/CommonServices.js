import {appDatabase} from "../configs/firebase";
import {child, get, push, ref} from "firebase/database"
import {objectToArray} from "../helpers/Helpers";
import {UserModel} from "../models/UserModel";

export class CommonServices {
    async writeUserData({name, role, profile_picture}) {
        await push(ref(appDatabase, "/users"), {
            name: name,
            role: role,
            profile_picture: profile_picture
        })
    }

    getUserData({setUsers}) {
        get(child(ref(appDatabase), '/users')).then((records) => {
            const usersData = [];

            if (records.exists()) {
                objectToArray(records.val()).forEach((record,) => {
                    usersData.push(new UserModel(record))
                })
            }

            setUsers(usersData);
        }).catch(() => {
            setUsers([])
        })
    }
}